module Student where



data Marks = Marks {
dipl_philosophy :: Float,
dipl_physics :: Float,
dipl_chemistry :: Float
} deriving (Show, Ord, Eq)



m = Marks {dipl_philosophy=4, dipl_physics=1, dipl_chemistry=6} 
